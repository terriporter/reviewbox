<?php
include "header.php";
include "config/init.php";
include "functions/series.php";
include "functions/validate.php";
include "nav.php";

?>

<h2>Our TV Series</h2>
<p>Welcome, this website is about giving you suggestions on TV Shows to watch during relaxation time. </p>

<p>Select a Genre: 
<a href="series_table.php?series_genre=Medical Drama">Medical Drama</a>
<a href="index.php?series_genre=Supernatural Teen Drama">Supernatural Teen Drama</a>
<a href="index.php?series_genre=Serial Drama">Serial Drama</a>
<a href="index.php?series_genre=Sitcom">Sitcom</a>
<a href="index.php?series_genre=Action">Action</a>
<a href="index.php">All Genres</a>
</p>



<?php
	
 $series = new series;
 if(!isset($_GET["series_genre"])){
 $series_array = $series->series_results($handler);
 } else {
 	$genre = validate_input($_GET["series_genre"]);
 	$series_array = $series->genre_search($handler, $genre);
 }

 ?>


<p>Click on a Series to display their information.</p>
<p id='showSeries'></p>
<table id="demo"></table>

<script>
var x,xmlhttp,xmlDoc
xmlhttp = new XMLHttpRequest();
xmlhttp.open("GET", "series.xml", false);
xmlhttp.send();
xmlDoc = xmlhttp.responseXML; 
x = xmlDoc.getElementsByTagName("series");
table="<tr><th>name</th><th>image</th><th>date_released</th><th>overview</th><th>description</th><th>genre</th></tr>";
for (i = 0; i <x.length; i++) { 
  table += "<tr onclick='displaySeries(" + i + ")'><td>";
  table += x[i].getElementsByTagName("name")[0].childNodes[0].nodeValue;
  table += "</td><td>";
  table +=  x[i].getElementsByTagName("image")[0].childNodes[0].nodeValue;
  table += "</td><td>";
  table += x[i].getElementsByTagName("date_released")[0].childNodes[0].nodeValue;
  table += "</td><td>";
  table += x[i].getElementsByTagName("overview")[0].childNodes[0].nodeValue;
  table += "</td><td>";
  table += x[i].getElementsByTagName("description")[0].childNodes[0].nodeValue;
  table += "</td><td>";
  table += x[i].getElementsByTagName("genre")[0].childNodes[0].nodeValue;
  table += "</td></tr>";
}
document.getElementById("demo").innerHTML = table;

function displaySeries(i) {
  document.getElementById("showSeries").innerHTML =
  "Name: " +
  x[i].getElementsByTagName("name")[0].childNodes[0].nodeValue +
  "<br>Image: " +
  x[i].getElementsByTagName("image")[0].childNodes[0].nodeValue +
  "<br>date_released: " + 
  x[i].getElementsByTagName("date_released")[0].childNodes[0].nodeValue +
  "<br>overview: " + 
  x[i].getElementsByTagName("overview")[0].childNodes[0].nodeValue +
  "<br>description: " + 
  x[i].getElementsByTagName("description")[0].childNodes[0].nodeValue +
  "<br>genre: " + 
  x[i].getElementsByTagName("genre")[0].childNodes[0].nodeValue;
}
</script>


<?php
include "footer.php";
?>