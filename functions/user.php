<?php

include "config/init.php";

class user {

	private $loggedin;
	private $forename;
	private $surname;
	private $email;
	private $password_hash;

	public function login_status(){
		return $loggedin;
	}
	
	public function set_forename($forename){
		if (!empty($forename)) {
			$this->forename = $forename;
			return true;
		} else {
			return false;
		}
	}

	public function set_surname($surname){
		if (!empty($surname)) {
			$this->surname = $surname;
			return true;
		} else {
			return false;
		}
		
	}

	public function set_email($email){
		if(!empty($email)){
			$this->email = $email;
			return true;	
		} else {
			return false;
		}
		
	}

	public function set_password_confirmation($password, $conf_password){
		if(!empty($password)){
			if($password == $conf_password){
				$this->password_hash = password_hash($password, PASSWORD_DEFAULT);
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	public function set_password($password){
		if(!empty($password)){
			$this->password_hash = password_hash($password, PASSWORD_DEFAULT);
			return true;
		} else {
			return false;
		}
	}

	public function get_forename(){
		return $this->forename;
	}

	public function get_surname(){
		return $this->surname;
	}

	public function get_email(){
		return $this->email;
	}

	public function check_password(){
		//check the users password
	}

	public function get_fullname(){
		return $this->forename." ".$this->surname;
	}

	public function create_userdb($handler, $user_password){
		if(!empty($this->forename)){
			if(!empty($this->surname)){
				if(!empty($this->email)){
					if(!empty($user_password)){

						$new_pass_hash = password_hash($user_password, PASSWORD_DEFAULT);

						$ur = $handler->prepare("INSERT INTO users (forename, surname, email, password) VALUES ('".$this->forename."', '".$this->surname."', '".$this->email."','".$new_pass_hash."')");
						$ur->execute();
						$this->loggedin = true;
						return true;

					} else {
						return false;
					}
				} else {
					return false;
				}

			} else {
				return false;
			}
		} else {
			return false;
		}
	} 

	public function login_userdb($handler, $user_password_attempt){

                        $lr = $handler->prepare("SELECT * FROM users WHERE email = :email");
                        $lr->bindParam(':email', $this->email, PDO::PARAM_STR);
                        $lr->execute();

                        $email = "";
                        $forename = "";
                        $surname = "";
                        $password = "";

                        while($row = $lr->fetch()){
                            //var_dump($row);
                            $email = $row["email"];
                            $forename = $row["forename"];
                            $surname = $row["surname"];
                            $password = $row["password"];
                        }
                        
                        if(password_verify($user_password_attempt, $password)){
                            //var_dump($password);
                            $this->email = $email;
                            $this->forename = $forename;
                            $this->surname = $surname;
                            //$this->password = $password;
                            $this->loggedin = true;
                            return true;
                        } else {
                            return false;
                        }


    } //end of function

	public function update_details_db ($handler) {

		$upr = $handler->prepare("UPDATE users set forename = :forename, surname = :surname WHERE email = :email");
						$upr->bindParam(':forename', $this->forename, PDO::PARAM_STR);
						$upr->bindParam(':surname', $this->surname, PDO::PARAM_STR);
						$upr->bindParam(':email', $this->email, PDO::PARAM_STR);
						$upr->execute();
						return true;
	}

	public function update_paswd_db ($handler,$password,$conf_password) {

		if(!empty($password)){
			if($password == $conf_password){
				$this->password_hash = password_hash($password, PASSWORD_DEFAULT);

						$upr = $handler->prepare("UPDATE users set password = :password WHERE email = :email");
						$upr->bindParam(':password', $this->password_hash, PDO::PARAM_STR);
						$upr->bindParam(':email', $this->email, PDO::PARAM_STR);
						$upr->execute();


				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}

		
	}


} //end of class

 ?>