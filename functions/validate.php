<?php

function validate_input($string) {

    $string = trim($string);
    $string = strip_tags($string); 
    $string = addslashes($string); 
    $string = htmlentities($string);
    $string = htmlspecialchars($string);
    $string = filter_var($string, FILTER_SANITIZE_SPECIAL_CHARS);
    return $string;
}

?>