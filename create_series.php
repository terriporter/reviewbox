<?php
include "header.php";
include "config/init.php";
include "nav.php";
include "functions/series.php";
include "functions/validate.php";
?>

<div class="sixteen columns">

	<h1>Create Series:</h1>
				<form action="creating_series.php" method="POST">
					<label for="series_name">Series Name: *</label>
					<input type="text" id="series_name" name="series_name"/>
					<br>	
					<label for="series_image">Series Image: *</label>
					<input type="text" id="series_image" name="series_image"/>
					<br>
					<label for="series_date">Air Date: *</label>
					<input type="date" id="series_date" name="series_date"/>
					<br>
					<label for="description">Description:</label>
					<input type="text" id="description" name="description" maxlength = "255" size="150"/>
					<br>
					<label for="overview">Overview:</label>
					<input type="text" id="overview" name="overview" maxlength="255" size="150"/>
					<br>
					<label for="series_genre">Genre:</label>
					<input type="text" id="series_genre" name="series_genre"/>
					<br>
					<button type="submit" name="Submit" value="Submit">Submit </button>
				</form>	
			</div>	





<?php
include "footer.php";
?>