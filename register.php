<?php
include "header.php";
include "config/init.php";
include "nav.php";
?>

<?php

?>
	<div class="sixteen columns">
		<h2>Register Here: </h2>
			<p>Please fill in this form to register with us! </p>

			<div class="sixteen columns">
				<form action="signup_response.php" method="POST">
					<label for="user_forename">Forename: *</label>
					<input type="text" id="user_forename" name="user_forename"/>
					<br>	
					<label for="user_surname">Surname: *</label>
					<input type="text" id="user_surname" name="user_surname"/>
					<br>
					<label for="user_email">Email Address: *</label>
					<input type="email" id="user_email" name="user_email"/>
					<br>
					<label for="user_password">Password: *</label>
					<input type="password" id="user_password" name="user_password"/>
					<br>
					<label for="user_conf_pass">Confirm Password: *</label>
					<input type="password" id="user_conf_pass" name="user_conf_pass"/>
					<br>
					<div class="g-recaptcha" data-sitekey="6LcusDsUAAAAAOK2D0Wdf7Xb67lOvWvaVpbDeErj"></div>
					<button type="submit" name="Submit" value="Submit">Submit </button>
					
				</form>	
			</div>	

			<div class="sixteen columns">

			<?php
				if(!isset($_SESSION['user_error'])){
					$_SESSION["user_error"] = "Please press submit once you have registered.";
				} 
				echo "<p>".$_SESSION['user_error']."</p>";

			?>

			</div>

	</div>
<?php
include "footer.php";
?>