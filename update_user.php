<?php
include "functions/user.php";
include "config/init.php";
include "functions/validate.php";


if(isset($_POST["Submit"])){

	//taking the data from the update
	$user_forename = $_POST["user_forename"];
	$user_surname = $_POST["user_surname"];
	$user_email = $_POST["user_email"];
	$user_password = $_POST["user_password"];
	$user_conf_pass = $_POST["user_conf_pass"];

	filter_var($user_forename, FILTER_SANITIZE_STRING);
	filter_var($user_surname, FILTER_SANITIZE_STRING);
	filter_var($user_email, FILTER_SANITIZE_EMAIL);
	filter_var($user_password, FILTER_SANITIZE_STRING);
	filter_var($user_conf_pass, FILTER_SANITIZE_STRING);

	//sent the string to function to validate
	$user_forename = validate_input($user_forename);
	$user_surname = validate_input($user_surname);
	$user_email = validate_input($user_email);
	$user_password = validate_input($user_password);
	$user_conf_pass = validate_input($user_conf_pass);



	if($_SESSION['current_user'] -> set_forename($user_forename)) {
		if ($_SESSION['current_user'] -> set_surname($user_surname)) {
			if ($_SESSION['current_user'] -> update_details_db ($handler)) {
				$_SESSION["update_error"] = "Thank you, your details have been accepted";
			} else {
				$_SESSION["update_error"] = "Error upodaing details";
			}	
		} else {
			$_SESSION["update_error"] = "Sorry, an error occured whilst updating your Surname";
		}
	} else {
		$_SESSION["update_error"] = "Sorry, an error occured whilst updating your Forename";
	}



	if (!empty($user_password)){
		
			if ($_SESSION['current_user'] -> update_paswd_db ($handler, $user_password, $user_conf_pass )) {
				$_SESSION["update_error"] = "Thank you, your password has been accepted";
			} else {
				$_SESSION["update_error"] = "Error password details";
			}
		
	}

	if($_SESSION["update_error"] == "Please press submit once you have registered."){
		//header ('location: welcome.php');

		//resetting the session
			//session_unset();
	} else {
		header ('location: updatedetails.php');
	}


	

}


?>	