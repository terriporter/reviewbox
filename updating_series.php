<?php
include "functions/user.php";
include "config/init.php";
include "functions/validate.php";


if(isset($_POST["Submit"])){

	//taking the data from the update
	$series_id = $_POST["series_id"];
	$series_name = $_POST["series_name"];
	$series_image = $_POST["series_image"];
	$series_date = $_POST["series_date"];
	$description = $_POST["description"];
	$overview = $_POST["overview"];
	$series_genre = $_POST["series_genre"];

	//sent the string to function to validate
	$series_id = validate_input($series_id);
	$series_name = validate_input($series_name);
	$series_image = validate_input($series_image);
	$series_date = validate_input($series_date);
	$description = validate_input($description);
	$overview = validate_input($overview);
	$series_genre = validate_input($series_genre);
	
	$sr = $handler->prepare("UPDATE series SET name = :name, image = :image, date_released = :air_date, overview = :overview, description = :description, genre = :genre WHERE series_id = :id");
	$sr->bindParam(':id', $series_id, PDO::PARAM_INT);
	$sr->bindParam(':name', $series_name, PDO::PARAM_INT);
	$sr->bindParam(':image', $series_image, PDO::PARAM_INT);
	$sr->bindParam(':air_date', $series_date, PDO::PARAM_INT);
	$sr->bindParam(':description', $description, PDO::PARAM_INT);
	$sr->bindParam(':overview', $overview, PDO::PARAM_INT);
	$sr->bindParam(':genre', $series_genre, PDO::PARAM_INT);
	$sr->execute();

	header('location: admin_series.php');
}




?>	