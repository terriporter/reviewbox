<?php
//include connection.php
require "connection.php";

try{
	$handler = new PDO('mysql:host=localhost; dbname='.$db['database'],$db['username'],$db["password"]);
	$handler->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	
	} catch(PDOException $e){
	echo $e->getMessage();
	}

//testing the connection works
//echo "The rest of our page";

//start the session 
if(!isset($_SESSION)){
	session_start();
}

?>