<?php

include "header.php";
include "config/init.php";
include "nav.php";
include "functions/validate.php";
include "functions/sendemail.php";

if(isset($_POST["Submit"])){

	//taking the data from the form
	$client_name = $_POST["client_name"];
	$client_email = $_POST["client_email"];
	$client_feedback = $_POST["client_feedback"];

	//sent to function to validate
	$client_name = validate_input($client_name);
	$client_email = validate_input($client_email);
	$client_feedback = validate_input($client_feedback);

	//sanitizing the email
	filter_var($client_email, FILTER_SANITIZE_EMAIL);

	if(!empty($client_name)){

		if(!empty($client_email)){

			if(filter_var($client_email, FILTER_VALIDATE_EMAIL)){ 

				if(!empty($client_feedback)){

					//sending a thank you email 
					$mail = new send_mail();
					$mail -> set_dest($client_email, "Thank you for your Feedback");
					$mail -> set_content ("Thank you ".$client_name." for your feedback to www.terriporter.co.uk \n Here is a copy of your Feedback: ".$client_feedback."
						\n We will be reviewing your feedback soon. \n From Admin of www.terriporter.co.uk");
					$mail -> send();
					 ?>

					<div class="sixteen columns">
						<h2>Thank you! </h2>
						<p>We have noted your feedback, and an email has been sent to you with a copy. </p> 
					</div>

					<?php
					//connecting to the table
					$re = $handler->prepare("INSERT INTO website_feedback (client_name, client_email, client_feedback) VALUES ('$client_name', '$client_email', '$client_feedback')");
					$re->execute();

				} else {
				$_SESSION['error'] = 'Please enter Feedback.';
				echo "no feedback";
			}

			} else {
					$_SESSION['error'] = 'Please enter a valid Email.';
					echo "not an email";
				}

		} else {
				$_SESSION['error'] = 'Please enter your Email Address.';
				echo "no email";
			}

	} else {
				$_SESSION['error'] = 'Please enter your Name.';
				echo "no name";
			}


} else {

	echo "You did not click submit";
}

	if($_SESSION['error'] == "Please press submit once you have filled in the form."){
			//header ('location: formresponse.php');
			
			//resetting the session
				session_unset();

		 } else {
		 	echo "returned";
		 	header ('location: contact.php');
		 } 



?>