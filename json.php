<?php
include "config/init.php";


header("Content-type: application/json; charset=UTF-8");
$result = $handler->prepare("SELECT * FROM series");
$result->execute();

$total_series = $result->fetchAll(PDO::FETCH_ASSOC);
echo json_encode($total_series);


?>