<!doctype html>
<html lang="en">
<head>

  <meta charset="utf-8">
  <title>AID Assignment</title>
  <meta name="description" content=""/>
  <meta name="author" content=""/>


  <meta name="viewport" content="width=device-width, initial-scale=1"/>

  <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,400i,600" rel="stylesheet"/> 


  <link rel="stylesheet" href="assets/css/normalize.css"/>
  <link rel="stylesheet" href="assets/css/skeleton.css"/>
  <link rel="stylesheet" href="assets/css/main.css"/>
 

  <script src="http://code.jquery.com/jquery-latest.min.js"></script>
    <script src="includes/basic-jquery-slider-master/js/bjqs-1.3.min.js"></script>
    <link type="text/css" rel="Stylesheet" href="includes/basic-jquery-slider-master/bjqs.css" />
    <link type="text/css" rel="Stylesheet" href="includes/basic-jquery-slider-master/demo.css" />
  

  <!-- Favicon  
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <link rel="icon" type="image/png" href="assets/images/favicontv.png"/>

  <link href="http://api.openweathermap.org/data/2.5/forecast?id=524901&APPID=7ebd0192948eda99890eaf92237ee99c"/>


<!-- Google Recaptcha-->
<script src='https://www.google.com/recaptcha/api.js'></script>


<!-- adding the user function
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
<?php
  include "functions/user.php";

/*if(!isset($_SESSION['current_user'])){
  $current_user = new user();
  $current_user -> set_forename("Guest User");
  $_SESSION['current_user'] = $current_user;
} */

?>


</head>

<div class="sixteen columns">
<body>
  