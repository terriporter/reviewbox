<?php
include "header.php";
include "config/init.php";
include "nav.php";
?>

<script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC-S4zhhDSMxZSD2tS4wT7n329sL7hF5lM&callback=initMap">
    </script>

<h2>Our Location</h2>
<p>Below is the location of our Main Office in Leeds. Our Office is on Park Row, one of the main roads in Leeds so it can be quite busy. </p>
<p>To help anyone who is coming to visit us, we have added a Traffic Update on to the Map, giving you a head start on what the traffic will be like, thank you. </p>
    <div id="map"></div>
    <script>
      function initMap() {

        var uluru = {lat: 53.797920, lng: -1.547088};
        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 15,
          center: uluru
        });
        var marker = new google.maps.Marker({
          position: uluru,
          map: map,
          //draggable:true,
          title: 'Our Leeds Office'
        });

         var trafficLayer = new google.maps.TrafficLayer();
        trafficLayer.setMap(map);

      }
    </script>
    
<?php
include "footer.php";
?>