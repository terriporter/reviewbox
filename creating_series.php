<?php
include "functions/user.php";
include "config/init.php";
include "functions/validate.php";


if(isset($_POST["Submit"])){

	//taking the data from the insert
	$series_name = $_POST["series_name"];
	$series_image = $_POST["series_image"];
	$series_date = $_POST["series_date"];
	$description = $_POST["description"];
	$overview = $_POST["overview"];
	$series_genre = $_POST["series_genre"];

	//sent the string to function to validate
	$series_name = validate_input($series_name);
	$series_image = validate_input($series_image);
	$series_date = validate_input($series_date);
	$description = validate_input($description);
	$overview = validate_input($overview);
	$series_genre = validate_input($series_genre);
	
	$sr = $handler->prepare("INSERT INTO series (name, image, date_released, overview, description, genre) VALUES ('".$series_name."', '".$series_image."', '".$series_date."','".$overview."','".$description."','".$series_genre."')");
	$sr->execute();

	header('location: admin_series.php');
}




?>	