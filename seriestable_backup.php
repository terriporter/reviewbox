<?php
include "header.php";
include "config/init.php";
include "functions/series.php";
include "functions/validate.php";
include "nav.php";

?>

<h2>Our TV Series</h2>
<p>Welcome, this website is about giving you suggestions on TV Shows to watch during relaxation time. </p>

<p>Select a Genre: 
<a href="index.php?series_genre=Medical Drama">Medical Drama</a>
<a href="index.php?series_genre=Supernatural Teen Drama">Supernatural Teen Drama</a>
<a href="index.php?series_genre=Serial Drama">Serial Drama</a>
<a href="index.php?series_genre=Sitcom">Sitcom</a>
<a href="index.php?series_genre=Action">Action</a>
<a href="index.php">All Genres</a>
</p>



<?php
	
 $series = new series;
 if(!isset($_GET["series_genre"])){
 $series_array = $series->series_results($handler);
 } else {
 	$genre = validate_input($_GET["series_genre"]);
 	$series_array = $series->genre_search($handler, $genre);
 }


 //print_r($series_array);
echo '<table style="width:100%">';
	echo '<th>Series:</th>';
	echo '<th>Series Image:</th>';
	echo '<th>Air Date:</th>';
	echo '<th>Description:</th>';
	echo '<th>Overview:</th>';
	echo '<th>Series Genre:</th>';
for ($i=0; $i < count($series_array) ; $i++) { 
	//echo ('*** Item'.$i.'***');
	//print_r($series_array[$i]);
	echo '<tr>';
	/*echo '<td>';
	echo $series_array[$i][0];
	echo '</td>'; */
	echo '<td>';
	echo $series_array[$i][1];
	echo '</td>';
	echo '<td>';
	echo '<img src="assets/images/'.$series_array[$i][2].'" height="100" width="150">';
	echo '</td>';
	echo '<td>';
	echo $series_array[$i][3];
	echo '</td>';
	echo '<td>';
	echo $series_array[$i][4];
	echo '</td>';
	echo '<td>';
	echo $series_array[$i][5];
	echo '</td>';
	echo '<td>';
	echo $series_array[$i][6];
	echo '</td>';
	echo '</tr>';
	//echo ('*** END Item'.$i.'***');
}
echo '</table>';



include "footer.php";
?>