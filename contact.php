<?php
include "header.php";
include "config/init.php";
include "nav.php";
?>

<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.4/angular.min.js"></script>  
<body ng-app="">

<div class="sixteen columns">
	
<h2>Contact Us:</h2>
<p>Please fill in your details below to leave us a message:</p>


		<div class="ten columns">
			<form name="contact" action="formresponsetest.php" method="POST">
				<label for="client_name">Name: *</label>
				<input type="text" id="client_name" name="client_name" ng-model="client_name" required><span ng-show="contact.client_name.$touched && contact.client_name.$invalid">Your Name is required.</span>
				<br>
				<label for="client_email">Email Address: *</label>
				<input type="email" id="client_email" name="client_email"ng-model="client_email" required><span ng-show="contact.client_email.$touched && contact.client_email.$invalid">Your Email is required.</span>
				<br>
				<label for="client_feedback">Feedback: *</label>
				<input type="text" id="client_feedback" name="client_feedback" ng-model="client_feedback" required><span ng-show="contact.client_feedback.$touched && contact.client_feedback.$invalid">Your Feedback is required.</span>
				<br>
				<button type="submit" name="Submit" value="Submit">Submit </button>
			</form>
		</div>	
</div>	

<div class="sixteen columns">

<?php
	if(!isset($_SESSION['error'])){
		$_SESSION["error"] = "Please press submit once you have filled in the form.";
	} 
	echo "<p>".$_SESSION['error']."</p>";

?>

</div>

<?php
include "footer.php";
?>