<?php

class send_mail {
	
	public $to_email;
	public $mail_subject;
	public $mail_content;
	public $header = "From: donotreply@terriporter.co.uk";

	public function set_dest ($to_email, $mail_subject) {

		$this -> to_email = $to_email;
		$this -> mail_subject = $mail_subject;
	}

	public function set_content ($mail_content){

		$this -> mail_content = $mail_content;

	}


	public function send () {

		mail($this->to_email, $this->mail_subject, $this->mail_content, $this->header);
	}


}






?>