<?php
include "header.php";
include "config/init.php";
include "functions/series.php";
include "functions/validate.php";
include "nav.php";

if(!isset($_SESSION['current_user'])){
	$current_user = new user();
	$current_user -> set_forename("Guest User");
	$_SESSION['current_user'] = $current_user;
} 
?>


<h2 style="color:gray">Welcome to ReviewBox</h2>
<p>Our website is about giving you the latest information on the most popular Television Series which are available now. 
	We give you the information you want to know, and if you fancy even a sneak preview to get you hooked on watching the Show. 
	We keep our website up to date, so make sure to check regularly for the latest scoop on which Show to watch. 
	You can trust us on directing you to the right Television Series for you to watch whilst relaxing.</p>

	<div class="row">
		<div id="banner-fade">
			<ul class="bjqs">
				<li><img src="assets/banner-images/americangods.jpg" title="American Gods" alt="American Gods"></li>
				<li><img src="assets/banner-images/biglittlelies.jpg" title="Big Little Lies" alt="Big Little Lies"></li>
				<li><img src="assets/banner-images/mindhunter.jpg" title="Mind Hunter" alt="Mind Hunter"></li>
				<li><img src="assets/banner-images/riverdale.jpg" title="Riverdale" alt="Riverdale"></li>
				<li><img src="assets/banner-images/thedeuce.jpg" title="The Deuce" alt="The Deuce"></li>
				<li><img src="assets/banner-images/thegoodplace.png" title="The Good Place" alt="The Good Place"></li>
				<li><img src="assets/banner-images/twinpeaksthereturn.jpg" title="Twin Peaks the Return" alt="Twin Peaks the Return"></li>
			</ul>	
		</div>

		<script class="secret-source">
        		jQuery(document).ready(function($) {

          			$('#banner-fade').bjqs({
            		height      : 320,
            		width       : 620,
            		responsive  : true
          			});
        		});
      	</script>
	</div>

<?php
include "footer.php";
?>