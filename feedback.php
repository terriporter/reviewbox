<?php
include "header.php";
include "config/init.php";
include "nav.php";
include "functions/admin.php";

$admin_feedback = new admin;
$feedback_array = $admin_feedback->collect_feedback($handler);
?>

<h2>Feedback Received </h2>

<style>
table, th , td  {
  border: 1px solid grey;
  border-collapse: collapse;
  padding: 5px;
}
table tr:nth-child(odd) {
  background-color: #f1f1f1;
}
table tr:nth-child(even) {
  background-color: #ffffff;
}
</style>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.4/angular.min.js"></script>
<body>
 
<div ng-app="myApp" ng-controller="feedbackCtrl">
 
<?php
//print_r($feedback_array);
	echo '<table style="width:100%">';
	echo '<th>Date:</th>';
	echo '<th>Name:</th>';
	echo '<th>Email:</th>';
	echo '<th>Feedback:</th>';
for ($i=0; $i < count($feedback_array); $i++) { 
	echo '<tr>';
	echo '<td>';
	echo $feedback_array[$i][1];
	echo '</td>';
	echo '<td>';
	echo $feedback_array[$i][2];
	echo '</td>';
	echo '<td>';
	echo $feedback_array[$i][3];
	echo '</td>';
	echo '<td>';
	echo $feedback_array[$i][4];
	echo '</td>';
	echo '</tr>';
}
echo '</table>';

?>
 
</div>
 
<script>
var app = angular.module('myApp', []);
app.controller('feedbackCtrl', function($scope, $http) {
   $http.get("")
   .then(function (response) {$scope.names = response.data.records;});
});
</script>


<?php
include "footer.php";
?>