<?php
include "header.php";
include "config/init.php";
include "nav.php";
include "functions/series.php";
include "functions/validate.php";
?>

<div class="sixteen columns">

	<?php
	$id = validate_input($_GET["id"]);
	$sr = $handler->prepare("SELECT * FROM series WHERE series_id = :id");
	$sr->bindParam(':id', $id, PDO::PARAM_INT);
	$sr->execute();

	$seriesdata_array = $sr->fetchAll();

	//print_r($seriesdata_array);

	?>

	<h1>Update Details:</h1>
				<form action="updating_series.php" method="POST">
					<label for="series_id">Series ID: *</label>
					<input type="text" id="series_id" name="series_id" value="<?php echo $seriesdata_array['0']['0'] ?>" readonly/>
					<label for="series_name">Series Name: *</label>
					<input type="text" id="series_name" name="series_name" value="<?php echo $seriesdata_array['0']['1'] ?>"/>
					<br>	
					<label for="series_image">Series Image: *</label>
					<input type="text" id="series_image" name="series_image" value="<?php echo $seriesdata_array['0']['2'] ?>"/>
					<br>
					<label for="series_date">Air Date: *</label>
					<input type="date" id="series_date" name="series_date" value="<?php echo $seriesdata_array['0']['3'] ?>"/>
					<br>
					<label for="description">Description:</label>
					<input type="text" id="description" name="description" value="<?php echo $seriesdata_array['0']['4'] ?>" maxlength = "255" size="150"/>
					<br>
					<label for="overview">Overview:</label>
					<input type="text" id="overview" name="overview" value="<?php echo $seriesdata_array['0']['5'] ?>" maxlength="255" size="150"/>
					<br>
					<label for="series_genre">Genre:</label>
					<input type="text" id="series_genre" name="series_genre" value="<?php echo $seriesdata_array['0']['6'] ?>"/>
					<br>
					<button type="submit" name="Submit" value="Submit">Submit </button>
				</form>	
			</div>	





<?php
include "footer.php";
?>