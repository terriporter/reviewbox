<?php
include "header.php";
include "config/init.php";
include "nav.php";
include "functions/functions.php";

//header ('location: contact.php');

if(isset($_POST["Submit"])){

	//taking the data from the form
	$client_name = $_POST["client_name"];
	$client_email = $_POST["client_email"];
	$client_feedback = $_POST["client_feedback"];

	filter_var($client_name, FILTER_SANITIZE_STRING);
	filter_var($client_email, FILTER_SANITIZE_EMAIL);
	filter_var($client_feedback, FILTER_SANITIZE_STRING);

	//sent to function to validate
	$client_name = validate_input($client_name);
	$client_email = validate_input($client_email);
	$client_feedback = validate_input($client_feedback);


	if(!empty($client_name)){

		if(!empty($client_email)){

			if(!empty($client_feedback)){

				if(filter_var($client_email, FILTER_VALIDATE_EMAIL)){
				
					$subject = "Feedback from Message";
					$mail_content = "Client Name: ".$client_name."\n Client Email: ".$client_email."\n Client Message: ".$client_feedback;

					//invoke mail function
					mail($client_email, $subject, $mail_content);

					?>
					<div class="sixteen columns">
						<h2>Thank you! </h2>
						<p>We have noted your feedback, and an email has been sent to you with a copy. </p> 

					</div>	
					<?php
			
					//adding to the database
					$query = "INSERT INTO website_feedback (client_name, client_email, client_feedback) VALUES ('$client_name', '$client_email', '$client_feedback')";
					$result = mysqli_query($connection, $query);
					
				} else {
					$_SESSION['error'] = 'Please enter a valid Email.';
					echo "not an email";
				}		

			} else {
				$_SESSION['error'] = 'Please enter Feedback.';
				echo "no feedback";
			}
		} else {
				$_SESSION['error'] = 'Please enter your Email Address.';
				echo "no email";
			}

	} else {
				$_SESSION['error'] = 'Please enter your Name.';
				echo "no name";
			}	

} else {

	echo "You did not click submit";
}
	if($_SESSION['error'] == "Please press submit once you have filled in the form."){
		//header ('location: formresponse.php');
		
		//resetting the session
			session_unset();

	 } else {
	 	echo "returned";
	 	header ('location: contact.php');
	 } 

include "footer.php";
?>