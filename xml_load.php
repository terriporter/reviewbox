<?php
//header("Content-type: text/xml");

include "config/init.php";

$result = $handler->prepare("SELECT * FROM series");
$result->execute();

//$row = $result->fetchAll(PDO::FETCH_ASSOC);

//top of xml file
$_xml = '<?xml version="1.0" encoding="UTF-8"?>';
$_xml = '<?xml-stylesheet type="text/xsl"?>';


$_xml .="<full_series>";

while($row=$result->fetch()){
	$_xml .="<series>";
	$_xml .="<series_id>".$row['series_id']."</series_id>";
	$_xml .="<name>".$row['name']."</name>";
	$_xml .="<image>".$row['image']."</image>";
	$_xml .="<date_released>".$row['date_released']."</date_released>";
	$_xml .="<overview>".$row['overview']."</overview>";
	$_xml .="<description>".$row['description']."</description>";
	$_xml .="<genre>".$row['genre']."</genre>";
	$_xml .="</series>";
} 
	$_xml .= "</full_series>";


	//Parse and create an xml object using the string
	$xml = new SimpleXMLElement($_xml);
	//And output
	print $xml->asXML();
	//or we could write to a file
	//$xmlobj->asXML(series.xml);

?>