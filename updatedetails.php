<?php
include "header.php";
include "config/init.php";
include "nav.php";
?>

<div class="sixteen columns">

	<h1>Update Details:</h1>
				<form action="update_user.php" method="POST">
					<label for="user_forename">Forename: *</label>
					<input type="text" id="user_forename" name="user_forename" value="<?php echo $_SESSION['current_user'] -> get_forename(); ?>"/>
					<br>	
					<label for="user_surname">Surname: *</label>
					<input type="text" id="user_surname" name="user_surname" value="<?php echo $_SESSION['current_user'] -> get_surname(); ?>"/>
					<br>
					<label for="user_email">Email Address: *</label>
					<input type="email" id="user_email" name="user_email" value="<?php echo $_SESSION['current_user'] -> get_email(); ?>" readonly/>
					<br>
					<label for="user_password">Password:</label>
					<input type="password" id="user_password" name="user_password"/>
					<br>
					<label for="user_conf_pass">Confirm Password:</label>
					<input type="password" id="user_conf_pass" name="user_conf_pass"/>
					<br>
					<button type="submit" name="Submit" value="Submit">Submit </button>
				</form>	
			</div>	

				<div class="sixteen columns">
	<?php

			if(!isset($_SESSION['update_error'])){
					$_SESSION["update_error"] = "Please press submit once you have registered.";
				} 
				echo "<p>".$_SESSION['update_error']."</p>";

			?>

	</div>




<?php

include "footer.php";
?>