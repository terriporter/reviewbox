<?php

ob_start();
include "functions/user.php";
include "config/init.php";
include "functions/validate.php";

//print "hello";

//echo $_SESSION['current_user'];

if(isset($_POST["Submit"])){

	//taking the data from the register form
	$user_forename = $_POST["user_forename"];
	$user_surname = $_POST["user_surname"];
	$user_email = $_POST["user_email"];
	$user_password = $_POST["user_password"];
	$user_conf_pass = $_POST["user_conf_pass"];
	$captcha = $_POST['g-recaptcha-response'];

	filter_var($user_forename, FILTER_SANITIZE_STRING);
	filter_var($user_surname, FILTER_SANITIZE_STRING);
	filter_var($user_email, FILTER_SANITIZE_EMAIL);
	filter_var($user_password, FILTER_SANITIZE_STRING);
	filter_var($user_conf_pass, FILTER_SANITIZE_STRING);

	//sent the string to function to validate
	$user_forename = validate_input($user_forename);
	$user_surname = validate_input($user_surname);
	$user_email = validate_input($user_email);
	$user_password = validate_input($user_password);
	$user_conf_pass = validate_input($user_conf_pass);

	/*print $user_forename;
	print "</br>";
	print $user_surname;
	print "</br>";
	print $user_email;
	print "</br>";
	print $user_password;
	print "</br>";
	print $user_conf_pass;
	print "</br>";
	exit; */

	$response = json_decode(file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=6LcusDsUAAAAABrhLjXc30alJDfZS8tFo1wR_jRR&response=".$captcha."&remoteip".$_SERVER['REMOTE_ADDR'],false),true);
	print_r($response);
	//exit;

	if($response['success'] == true){
		echo "true";
			if($_SESSION['current_user'] -> set_forename($user_forename)){
				echo "forename";

				if($_SESSION['current_user'] -> set_surname($user_surname)){
					echo "surname";

					if($_SESSION['current_user'] -> set_email($user_email)){
						echo "email";

						if(filter_var($user_email, FILTER_VALIDATE_EMAIL)){
							echo "filter var";

							if($_SESSION['current_user'] -> set_password_confirmation($user_password, $user_conf_pass)){
								echo "password conf";
							//if($_SESSION['current_user'] -> set_password($user_password)){

								/*if($_SESSION['current_user'] -> create_userdb($handler, $user_password)){
									header ('location: signup_complete.php');

								} else {
									$_SESSION['user_error'] = "There has been an error signing you up, please send us a message on the contact page";
								} */

								$ur = $handler->prepare("INSERT INTO users (forename, surname, email, password) VALUES ('".$user_forename."', '".$user_surname."', '".$user_email."','".$user_password."')");
								$ur->execute();

								header ('Location: signup_complete.php');
								ob_flush();
								die();

							} else {
								$_SESSION['user_error'] = "There was an issue with your passwords, please check them.";
							}

						} else {
							$_SESSION['user_error'] = "Please enter a valid Email Address.";
						}

					} else {
						$_SESSION['user_error'] = "Please enter your Email Address.";
					}

				} else {
					$_SESSION['user_error'] = "Please enter your Surname.";
				}

			} else {
				$_SESSION['user_error'] = "Please enter your Forename.";
			}
	} else {
		$_SESSION['user_error'] = "There was an issue with the Google Recaptcha, we apologise for any inconvenience.".$response['success'];
	}		


} else {

	echo "You did not click submit";
}


	if($_SESSION["user_error"] == "Please press submit once you have registered."){
		//header ('location: welcome.php');

		//resetting the session
			//session_unset();
	} else {
		header ('Location: register.php');
		die();
	}

	?>