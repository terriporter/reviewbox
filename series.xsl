<?xml version="1.0" standalone="yes"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:output method="html" encoding="UTF-8" indent="yes"/>  
<xsl:template match="/">
<xsl:for-each select="full_series/series">
    <table class="table-paddings" cellspacing="0" cellpadding="0" border="1">      
        <tbody>
        <tr>
            <td width="245px">Series ID:</td>
            <td width="250px"><xsl:value-of select="series_id"/></td>
        </tr>
        <tr>
            <td width="245px">Series Name:</td>
            <td width="250px"><xsl:value-of select="name"/></td>
        </tr>
        <tr>
            <td width="245px">Series Image:</td>
            <td width="250px"><xsl:value-of select="image"/></td>
        </tr>
        <tr>
            <td width="245px">Date Released:</td>
            <td width="250px"><xsl:value-of select="date_released"/></td>
        </tr>
        <tr>
            <td width="245px">Series Overview:</td>
            <td width="250px"><xsl:value-of select="overview"/></td>
        </tr>
        <tr>
            <td width="245px">Series Description:</td>
            <td width="250px"><xsl:value-of select="description"/></td>
        </tr>
        <tr>
            <td width="245px">Series Genre:</td>
            <td width="250px"><xsl:value-of select="genre" /></td>
        </tr>
        </tbody>
    </table>
    <br/>
</xsl:for-each>
</xsl:template>
</xsl:stylesheet>

