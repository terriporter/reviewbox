<?php

 require "config/init.php";

class series {

	public $name;
	public $image;
	public $date_released;
	public $description;
	public $overview;
	public $genre;
	private $series_array;
	private $line_counter;

	public function setName($name){
		$this->name = $name;
	}
	
	public function setImage($image){
		$this->image = $image;
	}

	public function setDate_released($date_released){
		$this->date_released = $date_released;
	}

	public function setDescription($description){
		$this->description = $description;
	}

	public function setOverview($overview){
		$this->overview = $overview;
	}

	public function setGenre($genre){
		$this->genre = $genre;
	}

	public function getName(){
		return $this->name;
	}

	public function getImage(){
		return $this->image;
	}

	public function getDate_released(){
		return $this->date_released;
	}

	public function getDescription(){
		return $this->description;
	}

	public function getOverview(){
		return $this->overview;
	}

	public function getGenre(){
		return $this->genre;
	}

		public function series_results($handler){

		$sr = $handler->prepare("SELECT * FROM series");
		$sr->execute();

		/*while($row = $sr->fetch()){
			$this->name = $row["name"];
			$this->image = $row["image"];
			$this->date_released = $row["date_released"];
			$this->description = $row["description"];
			$this->overview = $row["overview"];
			$this->genre = $row["genre"];
		}  */

		/*$series_data = array();
		$series_data[0] = $this->name;
		$series_data[1] = $this->image;
		$series_data[2] = $this->date_released;
		$series_data[3] = $this->description;
		$series_data[4] = $this->overview;
		$series_data[5] = $this->genre; */

		//$i = "1";
		//while($i > 0){
		//$all_data = array( array($this->name, $this->image, $this->date_released, $this->description, $this->overview, $this->genre) ,
		 //array($this->name, $this->image, $this->date_released, $this->description, $this->overview, $this->genre));
		//$i++;
		//} 
		//return ($all_data);

		$seriesdata = array();
		/*while($line_data = $sr->fetch(PDO::FETCH_ASSOC)){
			$seriesdata[$line_data][$this->name] = $line_data['name'];
			$seriesdata[$line_data][$this->image] = $line_data['image'];
			$seriesdata[$line_data][$this->date_released] = $line_data['date_released'];
			$seriesdata[$line_data][$this->description] = $line_data['description'];
			$seriesdata[$line_data][$this->overview] = $line_data['overview'];
			$seriesdata[$line_data][$this->genre] = $line_data['genre'];

		}*/

		//print_r($line_data = $sr->fetchAll());
		$line_data = $sr->fetchAll();
		//print_r($line_data);
		return ($line_data);

	}

	public function addSeries(){

	}

	public function genre_search($handler, $seriesgenre){

		//var_dump($seriesgenre);

		$sr = $handler->prepare("SELECT * FROM series WHERE genre = :genre");
		$sr->bindParam(':genre', $seriesgenre, PDO::PARAM_INT);
		$sr->execute();

		$seriesdata = array();
		$line_data = $sr->fetchAll();

		//var_dump($line_data);
		return ($line_data);
}

}

?>